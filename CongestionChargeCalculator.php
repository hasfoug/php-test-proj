<?php

namespace App;

use App\Model\CalculatorInput;
use \DateInterval;
use \DateTime;

/**
 * Class CongestionChargeCalculator
 *
 * @package App
 */
class CongestionChargeCalculator
{
    /** @var array */
    private $weekends = ['Saturday', 'Sunday'];

    /**
     * Calculate congestion charge.
     *
     * @param  CalculatorInput $input
     *
     * @return array
     */
    public function calculateCharge(CalculatorInput $input)
    {
        $result = [];

        $rates = $this->getChargeRates($input->getVehicleType());
        $chargeTime = $this->calculateChargeTime($input->getStartTime(), $input->getEndTime());
        foreach ($chargeTime as $key => $time) {
            $result[$key] = [
                'time'   => $this->convertMinutesToHours($time),
                'charge' => number_format(round($time * ($rates[$key] / 60), 1,PHP_ROUND_HALF_DOWN), 2)
            ];
        }

        return $result;
    }

    /**
     * Get rate per hour based on vehicle type.
     *
     * @param  string $vehicleType
     *
     * @return array
     */
    private function getChargeRates($vehicleType)
    {
        switch ($vehicleType) {
            case 'Motorbike':
                return [
                    'AM' => 1,
                    'PM' => 1
                ];

            default:
                return [
                    'AM' => 2,
                    'PM' => 2.5
                ];
        }
    }

    /**
     * Calculate charge time in minutes for time period.
     *
     * @param  DateTime $startTime
     * @param  DateTime $endTime
     *
     * @return array
     */
    private function calculateChargeTime(DateTime $startTime, DateTime $endTime)
    {
        $amTime = $pmTime = 0;

        do {
            $diff = $startTime->diff($endTime);

            $weekday = $startTime->format('l');
            if (in_array($weekday, $this->weekends)) {
                $startTime->modify('+ 1 day');
                $startTime->setTime(0, 0, 0);

                continue;
            }

            $amChargeIntervalStart = clone($startTime);
            $amChargeIntervalStart->setTime(7,0,0);

            $amChargeIntervalEnd = clone($startTime);
            $amChargeIntervalEnd->setTime(12,0,0);

            $pmChargeIntervalStart = clone($startTime);
            $pmChargeIntervalStart->setTime(12,0,0);

            $pmChargeIntervalEnd = clone($startTime);
            $pmChargeIntervalEnd->setTime(19,0,0);

            if ($startTime->getTimestamp() <= $amChargeIntervalStart->getTimestamp() && $endTime->getTimestamp() >= $amChargeIntervalStart->getTimestamp()) {
                if ($endTime->getTimestamp() <= $amChargeIntervalEnd->getTimestamp())
                    $amChargeIntervalEnd = clone($endTime);

                $startTime->setTime(7, 0, 0);
                $amDiff = $startTime->diff($amChargeIntervalEnd);

                $amTime += $this->getIntervalMinutes($amDiff);
            } elseif ($startTime >= $amChargeIntervalStart && $startTime <= $amChargeIntervalEnd && $endTime >= $amChargeIntervalStart) {
                if ($endTime <= $amChargeIntervalEnd)
                    $amChargeIntervalEnd = clone($endTime);

                $amDiff = $startTime->diff($amChargeIntervalEnd);

                $amTime += $this->getIntervalMinutes($amDiff);
            }

            if ($startTime <= $pmChargeIntervalStart && $endTime >= $pmChargeIntervalStart) {
                if ($endTime <= $pmChargeIntervalEnd)
                    $pmChargeIntervalEnd = clone($endTime);

                $startTime->setTime(12, 0, 0);
                $pmDiff = $startTime->diff($pmChargeIntervalEnd);

                $pmTime += $this->getIntervalMinutes($pmDiff);
            } elseif ($startTime >= $pmChargeIntervalStart && $startTime <= $pmChargeIntervalEnd && $endTime >= $pmChargeIntervalStart) {
                if ($endTime <= $pmChargeIntervalEnd)
                    $pmChargeIntervalEnd = clone($endTime);

                $pmDiff = $startTime->diff($pmChargeIntervalEnd);

                $pmTime += $this->getIntervalMinutes($pmDiff);
            }

            $startTime->modify('+ 1 day');
            $startTime->setTime(0, 0, 0);
        } while($diff->d > 0);

        return [
            'AM' => $amTime,
            'PM' => $pmTime
        ];
    }

    /**
     * @param  DateInterval $interval
     *
     * @return int
     */
    private function getIntervalMinutes(DateInterval $interval)
    {
        return ($interval->h * 60 + $interval->i);
    }

    /**
     * @param  integer $minutes
     *
     * @return string
     */
    private function convertMinutesToHours($minutes)
    {
        $hours = floor($minutes / 60);
        $minutes = $minutes % 60;

        return sprintf('%dh %dm', $hours, $minutes);
    }

    /**
     * Print calculation result.
     *
     * @param array $result
     */
    public function printResult(array $result)
    {
        echo sprintf('Charge for %s (AM rate): %s%s', $result['AM']['time'], chr(163), $result['AM']['charge']).PHP_EOL;
        echo sprintf('Charge for %s (PM rate): %s%s', $result['PM']['time'], chr(163), $result['PM']['charge']).PHP_EOL;
        echo sprintf('Total charge: %s%s', chr(163), number_format($result['AM']['charge'] + $result['PM']['charge'], 2)).PHP_EOL.PHP_EOL;
    }
}