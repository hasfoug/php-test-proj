<?php

namespace App\Model;

use \DateTime;

/**
 * Class CalculatorInput
 *
 * @package App\Model
 */
class CalculatorInput
{
    /** @var string */
    private $vehicleType;

    /** @var DateTime */
    private $startTime;

    /** @var DateTime */
    private $endTime;

    /**
     * CalculatorInput constructor.
     *
     * @param $vehicleType
     * @param DateTime $startTime
     * @param DateTime $endTime
     */
    public function __construct($vehicleType, DateTime $startTime, DateTime $endTime)
    {
        $this->vehicleType = $vehicleType;
        $this->startTime   = $startTime;
        $this->endTime     = $endTime;
    }

    /**
     * @return string
     */
    public function getVehicleType()
    {
        return $this->vehicleType;
    }

    /**
     * @return DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @return DateTime
     */
    public function getEndTime()
    {
        return $this->endTime;
    }
}