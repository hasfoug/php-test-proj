<?php

namespace App;

require_once('CongestionChargeCalculator.php');
require_once('Model/CalculatorInput.php');

use \App\Model\CalculatorInput;
use \DateTime;

$calculator = new CongestionChargeCalculator();

$input1 = new CalculatorInput('Car', new DateTime('2008-04-24T11:32:00.000000Z'), new DateTime('2008-04-24T14:42:00.000000Z'));
$input2 = new CalculatorInput('Motorbike', new DateTime('2008-04-24T17:00:00.000000Z'), new DateTime('2008-04-24T22:11:00.000000Z'));
$input3 = new CalculatorInput('Van', new DateTime('2008-04-25T10:23:00.000000Z'), new DateTime('2008-04-28T09:02:00.000000Z'));

$result1 = $calculator->calculateCharge($input1);
$calculator->printResult($result1);

$result2 = $calculator->calculateCharge($input2);
$calculator->printResult($result2);

$result3 = $calculator->calculateCharge($input3);
$calculator->printResult($result3);


